$(document).ready(
    resize
);
//$(document).ready(function(){
//    $(".tab-content").css("height", window.innerHeight-50);
//});
$(window).resize(
    resize
);
function resize() {
    var screen_width = window.innerWidth;
    $(".main").css("width", screen_width - 300 - screen_width * 0.05);
    $(".top").css("width", screen_width - 300 - screen_width * 0.05 + 16);
    $(".tab-content").css("min-height", window.innerHeight - 50);
}
/** slide in menu for editor*/
$("#toggle").click(function () {
    $("#brand").toggle("slide", {direction: 'down', mode: 'show'}, 200);
    $(".menu").toggle("slide", {direction: 'right', mode: 'show'}, 200);
});
$("#searchbox").focus(function () {
    $("#brand").hide();
}).blur(function () {
    $("#brand").show();
    //$("#brand").css("margin-left", "-200px");
});
$("#editor").focus(function () {
    showMenu();
}).blur(function () {
    //hideMenu();
});
function showMenu() {
    $("#brand").hide("slide", {direction: 'down', mode: 'show'}, 200);
    $(".menu").show("slide", {direction: 'right', mode: 'show'}, 200);
}
function hideMenu() {
    $("#brand").show("slide", {direction: 'down', mode: 'show'}, 200);
    $(".menu").hide("slide", {direction: 'right', mode: 'show'}, 200);
}
//popovers
var buttons = [
    {name: "newNote", message: "New Note(CTRL+N)"},
    {name: "searchIcon", message: "Search"},
    {name: "bold", message: "Bold (CTRL+B)"},
    {name: "italic", message: "Italic (CTRL+I)"},
    {name: "underline", message: "Underline(CTRL+U)"},
    {name: "strikethrough", message: "Strikethrough"},
    {name: "insertunorderedlist", message: "Bulleted List"},
    {name: "insertorderedlist", message: "Bulleted List"},
    {name: "math", message: "Insert Math"},
    //{name: "code", message: "Insert Code"},
    {name: "toggle", message: "Edit"},
    {name: "alignLeft", message: "Align Left", placement: "right"},
    {name: "alignCenter", message: "Align Center", placement: "right"},
    {name: "alignRight", message: "Align Right", placement: "right"}
    //{name: "insertheader", message: "h1 \n h2 \n h3 \n h4 \n h5 \n h6"}
];
$(document).ready(function () {
    $.each(buttons, function (i) {
        $("#" + buttons[i].name).popover(getPopover(buttons[i]));
    });
});
function getPopover(obj) {
    var place;
    if (obj.placement === undefined) {
        place = "bottom";
    }
    else{
        place = obj.placement;
    }
    return {
        html: 'true',
        content: '<div>' + obj.message + '</div>',
        trigger: "hover",
        placement: place,
        container: "body"
    }
}
var editorDoc;
function InitEditable() {
    var editor = document.getElementById("editor");
    if (editor.contentDocument)
        editorDoc = editor.contentDocument;
    else
        editorDoc = editor.contentWindow.document;

    var editorBody = editorDoc.body;

    // turn off spellcheck
    if ('spellcheck' in editorBody) {    // Firefox
        editorBody.spellcheck = false;
    }

    if ('contentEditable' in editorBody) {
        // allow contentEditable
        editorBody.contentEditable = true;
    }
    else {  // Firefox earlier than version 3
        if ('designMode' in editorDoc) {
            // turn on designMode
            editorDoc.designMode = "on";
        }
    }
}
function toggle(condition, option) {
    document.execCommand(condition, false, option);
}
var conditions = ["bold", "italic", "underline", "strikethrough", "insertunorderedlist", "insertorderedlist","justifyLeft","justifyCenter","justifyRight"];
var alignObj = {justifyLeft: "<i class='fa fa-align-left fa-2x'></i>",justifyCenter: "<i class='fa fa-align-center fa-2x'></i>",justifyRight: "<i class='fa fa-align-right fa-2x'></i>"};
var align = ["justifyLeft","justifyCenter","justifyRight"];
function checkSelection() {
    var toReturn = [];
    $.each(conditions, function (i) {
        if (document.queryCommandState) {
            if (document.queryCommandState(conditions[i])) {
                toReturn.push(conditions[i]);
            }
        }
    });
    return toReturn;
}
var primary = "white";
var alt = "#c4e1f2";
$("#editor").bind("DOMSubtreeModified", colorButton);
function colorButton() {

    var toCheck = checkSelection();
    $.each(conditions, function (i) {
        var id = "#" + conditions[i];
        if ($.inArray(conditions[i], toCheck) != -1) {
            changeColor(id, primary);
        }
        else if ($.inArray(align[i], toCheck) != -1) {
            $("#align").html(alignObj[align[i]]+"<i class='fa fa-angle-down fa-2x center'></i>");
        }
        else {
            changeColor(id, alt);
        }
    });

}
function changeColor(id, val) {
    $(id).css("color", val);
}
/** tabs */
$(document).ready(function () {
    $('.tabs .tab-links a').on('click', function (e) {
        var currentAttrValue = $(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

        // Change/remove current tab to active
        $(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
});
/** Helper function to get object keys */
if (typeof Object.keys !== "function") {
    (function() {
        Object.keys = Object_keys;
        function Object_keys(obj) {
            var keys = [], name;
            for (name in obj) {
                if (obj.hasOwnProperty(name)) {
                    keys.push(name);
                }
            }
            return keys;
        }
    })();
}