function backSpace()
{
    p = document.getElementById ("editor");
    c = getCaretPosition(p);console.log( getCaretPosition(p));
    str= $("#editor").html();
    if(c>0 && c<= str.length){
      $("#editor").focus().html(str.substring(0,c-1)+ str.substring(c,str.length));

//     console.log( c-1 );
  //$(p).focus().setCursorPosition(c-1);

  p.focus();
var textNode = p.firstChild;
var caret = c-1 ; // insert caret after the 10th character say
var range = document.createRange();
range.setStart(textNode, caret);
range.setEnd(textNode, caret);
var sel = window.getSelection();
sel.removeAllRanges();
sel.addRange(range);
    }
}

$.fn.setCursorPosition = function(pos) {
  this.each(function(index, elem) {
    if (elem.setSelectionRange) {
      elem.setSelectionRange(pos, pos);
    } else if (elem.createTextRange) {
      var range = elem.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
  return this;
};

function getCaretPosition(editableDiv) {
  var caretPos = 0,
sel, range;
  if (window.getSelection) {
sel = window.getSelection();
if (sel.rangeCount) {
  range = sel.getRangeAt(0);
  if (range.commonAncestorContainer.parentNode == editableDiv) {
    caretPos = range.endOffset;
  }
}
  } else if (document.selection && document.selection.createRange) {
range = document.selection.createRange();
if (range.parentElement() == editableDiv) {
  var tempEl = document.createElement("span");
  editableDiv.insertBefore(tempEl, editableDiv.firstChild);
  var tempRange = range.duplicate();
  tempRange.moveToElementText(tempEl);
  tempRange.setEndPoint("EndToEnd", range);
  caretPos = tempRange.text.length;
}
  }
  return caretPos;
}