/** Global variables */
var address = "https://api.smartnot.es/";
var auth = $.cookie("auth");
var noteID;
/** Create a new note */
$("#newNote").click(function () {
    newNote('newNote')
});
function newNote(option) {
    if (option === 'newNote') {
        $.ajax({
            type: 'POST',
            url: address + "note",
            headers: {
                "Authorization": auth
            }
        }).done(function (response, query) {
            clearEditor();
            noteID = response.id;
            setHash(noteID);
        });
    }
    else if (option == 'existing') {
        $.ajax({
            type: 'POST',
            url: address + "note",
            headers: {
                "Authorization": auth
            }
        }).done(function (response, query) {
            saveNote;
            noteID = response.id;
            setHash(noteID);
        });
    }
}
/** Retrieve all of a user's notes and display them in the sidebar */
var notes = [];
$(document).ready(function () {
    if (getHash() !== ""){
        loadNote(getHash().substring(1, getHash().length));
    }
    $.ajax({
        type: 'GET',
        url: address + "note",
        //async: false,
        headers: {
            "Authorization": auth
        }
    }).done(function (response) {
        //create as many as you need
        $.each(response.notes, function (i) {

            $.ajax({
                type: 'GET',
                url: address + "note/" + response.notes[i].id,
                async: false,
                headers: {
                    "Authorization": auth
                }
            }).done(function (data) {
                var noteText = data.text;
                var date = parseDate(data.date_created);
                var htmlString = createNoteEntry(i, response.notes[i].title, noteText, date);
                var temp = $(htmlString).attr("data-note-ID", data.id);
                htmlString = temp[0];
                notes.push({unixDate: unixDate(data.date_created), toAppend: htmlString});
            });
        });

    }).always(function () {
        notes.sort(compare);
        for (var j = notes.length - 1; j >= 0; j--) {
            $("#tab1").append(notes[j].toAppend);
        }
        $("#tab1").children().click(function () {
            $(this).toggleClass("current");
            $(this).toggleClass("inactive");
            loadNote($(this).attr("data-note-ID"));
            $("#note_0 > hr").toggle();
        }).mouseover(function () {
            $(this).addClass("mouse-hover");
            $(this).addClass("current");
            $(this).removeClass("inactive");
            $("#note_0_post").hide();
            $(this).css("cursor", "pointer");
        }).mouseout(function () {
            $(this).removeClass("mouse-hover");
            $(this).removeClass("current");
            $(this).addClass("inactive");
            $("#note_0_post").show();
        });
    });
});
/** Returns html toAppend to the sidebar */
function createNoteEntry(i, title, text, date) {
    return "<div id='note_" + i + "'class='container inactive'>" + "<p class='header'>" + title + "</p><p class='date'>" + date + "</p><p class='notePreview'>" +
        text + "</p></div>";
}
/** Parses date as relative time from now */
function parseDate(unformattedStr) {
    return moment(unformattedStr, "ddd, DD MMM YYYY HH:mm:ss Z").fromNow();
}
/** Parses date as unix epoch */
function unixDate(unformattedStr) {
    return moment(unformattedStr, "ddd, DD MMM YYYY HH:mm:ss Z").unix();
}
/** Comparable */
function compare(a, b) {
    if (a.unixDate < b.unixDate)
        return -1;
    else if (a.unixDate > b.unixDate)
        return 1;
    else
        return 0;
}
/** Returns text of a note given as parameter */
function retrieveText(noteID) {
    console.log(noteID);
    var noteText;
    $.ajax({
        type: 'GET',
        url: address + "note/" + noteID,
        headers: {
            "Authorization": auth
        }
    }).done(function (response) {
        //console.log(response.text);
        noteText = response.text;
    });
    return noteText;
}
/** Loads a note with the given id */
function loadNote(id) {
    noteID = id;
    $.ajax({
        type: 'GET',
        url: address + "note/" + id,
        headers: {
            "Authorization": auth
        }
    }).done(function (response) {
        var obj = JSON.parse(response.text);
        load(response.title, obj.array, obj.mathCode);
        setHash(noteID);
        //noteText = response.text;
    }).fail(function(response){
        //TODO: redirect to 403 page
        console.log("403 foribdden");
    });
}
/** Saves note every timeStep seconds */
var timeStep = 1000;
var oldObj = null;
var newObj = null;
var dmp = new diff_match_patch();
window.setInterval(saveNote, timeStep);
function saveNote() {
    oldObj = newObj;
    var temp = save();
    newObj = JSON.stringify(temp);
    if ((oldObj != null && newObj != null) && (oldObj !== newObj)) {
        var patch = dmp.patch_make(oldObj, newObj);
        //TODO: uncomment line below once diffs implemented on backend
        //var toSend = dmp.patch_toText(patch);
        var title;
        if ($(".title").text().length == 0) {
            title = "Untitled"
        }
        else {
            title = $(".title").text();
        }
        var toSend = newObj;
        $.ajax({
            type: 'PUT',
            url: address + "note/" + noteID,
            headers: {
                "Authorization": auth
            },
            data: {
                title: title,
                text: toSend
            }
        }).done(function (response) {
            console.log("saved");
        }).fail(function () {
            newNote('existing');
        });
    }
}
function setHash(noteID) {
    window.location.hash = noteID;
}
function getHash() {
    return window.location.hash
}